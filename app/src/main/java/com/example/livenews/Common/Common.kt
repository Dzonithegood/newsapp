package com.example.livenews.Common

import com.example.livenews.Interface.NewsService
import com.example.livenews.Remote.RetrofitClient

object Common {
    val BASE_URL = "https://newsapi.org/"
    val API_KEY = "9efb627ed9e8417983fcd3230c3e2915"

    val newsService:NewsService
    get()=RetrofitClient.getClient(BASE_URL).create(NewsService::class.java)

    fun getNewsAPI(source:String):String {
        val apiUrl =
            StringBuilder("https://newsapi.org/v2/top-headlines?sources=bbc-news&apiKey=9efb627ed9e8417983fcd3230c3e2915")
                .append(source)
                .append("&apiKey=")
                .append(API_KEY)
                .toString()
        return apiUrl


    }
}