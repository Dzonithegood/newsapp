package com.example.livenews

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.livenews.Adapter.ListSourceAdapter
import com.example.livenews.Common.Common
import com.example.livenews.Interface.NewsService
import com.example.livenews.Model.WebSite
import com.google.gson.Gson
import dmax.dialog.SpotsDialog
import io.paperdb.Paper
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var mService: NewsService
    lateinit var adapter: ListSourceAdapter
//    lateinit var dialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//Init cache DB
        Paper.init(this)
        //Init Service
        mService = Common.newsService
        //Init View
        swipe_to_refresh.setOnRefreshListener {
            loadWebSiteSource(true)
        }
        recycler_view_source_news.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        recycler_view_source_news.layoutManager = layoutManager

//        dialog=SpotsDialog(this)

        loadWebSiteSource(false)

    }

    private fun loadWebSiteSource(isRefresh: Boolean) {

        if(!isRefresh){
            //read cache
            val cache = Paper.book().read<String>("cache")
            if(cache !=null && !cache.isBlank() && cache != "null"){
                val webSite= Gson().fromJson<WebSite> (cache,WebSite::class.java)
                adapter = ListSourceAdapter(this,webSite)
                adapter.notifyDataSetChanged()
                recycler_view_source_news.adapter=adapter
            }else{
                //Load website and write cache
//                dialog.show()
                //Fetch new data
                mService.sources.enqueue(object: retrofit2.Callback<WebSite>{
                    override fun onFailure(call: Call<WebSite>, t: Throwable) {
//                        Toast.makeText(this,"Failed",Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<WebSite>, response: Response<WebSite>) {
                      adapter= ListSourceAdapter(this@MainActivity,response!!.body()!!)
                        adapter.notifyDataSetChanged()
                        recycler_view_source_news.adapter=adapter
                        //Save to cache
                        Paper.book().write("cache",Gson().toJson(response!!.body()!!))
//                        dialog.dismiss()
                    }


                })
            }
        }
        else{
            swipe_to_refresh.isRefreshing=true
            //Fetch new data
            mService.sources.enqueue(object: retrofit2.Callback<WebSite>{
                override fun onFailure(call: Call<WebSite>, t: Throwable) {
                    Toast.makeText(baseContext,"Failed",Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<WebSite>, response: Response<WebSite>) {
                    adapter= ListSourceAdapter(this@MainActivity,response!!.body()!!)
                    adapter.notifyDataSetChanged()
                    recycler_view_source_news.adapter=adapter
                    //Save to cache
                    Paper.book().write("cache",Gson().toJson(response!!.body()!!))
                    swipe_to_refresh.isRefreshing=false

                }

            })
        }
    }
}
