package com.example.livenews.Interface

import android.provider.ContactsContract
import com.example.livenews.Model.News
import com.example.livenews.Model.WebSite
import retrofit2.Call

import retrofit2.http.GET
import retrofit2.http.Url

interface NewsService {

@get:GET("v2/sources?apiKey=9efb627ed9e8417983fcd3230c3e2915")

val sources: Call<WebSite>

@GET
fun getNewsFromSource(@Url url: String):Call<News>

}